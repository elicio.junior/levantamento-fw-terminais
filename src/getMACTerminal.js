require('dotenv/config')
const fs = require('fs')
const readline = require('readline-sync')
const puppeteer = require('puppeteer')

const robots = { 
  input: require('./input.js'),
  state: require('./state.js')
}

function timestampData(){
  let now = new Date()
  let currentDay = now.getDate()
  let currentDayOfWeek = now.getDay()
  let currentYear  = now.getFullYear()
  let currentMonth = (now.getMonth() + 1).toString().padStart(2, "0")
  let hour  = '17:00:00'
  
  if(currentDayOfWeek > "4"){
    let diff = currentDayOfWeek - 4
    let dayOfLastThrusday = currentDay - diff
    let searchDate = currentYear+'-'+currentMonth+'-'+dayOfLastThrusday+' '+hour
    console.log(`Data de pesquisa: ${searchDate}`)
    let date = Date.parse(searchDate)
    let timestamp = date/1000
    return timestamp
  }else{
    let now = new Date();
    let today = now.getDay()
    let month = now.getMonth()
    let nextThursdayDiff = 4 - today
    let nextThursday = now.getDate() + nextThursdayDiff
    let searchDate = new Date(now.getFullYear(),now.getMonth(),nextThursday-7)
    console.log(`Data de pesquisa: ${searchDate}`)
    let date = Date.parse(searchDate)
    let timestamp = (date/1000)+61200   
    return timestamp
  }
}

const acessaDatacenter = async () => {
  try{
    const content = robots.state.load()
    console.log(`Criando o navegador...`)
    const browser = await puppeteer.launch({
      headless: true,
      ignoreSSL: true,
      ignoreHTTPSErrors: true,
      args: [
        '--start-maximized',
        '--disable-setuid-sandbox',
        '--no-sandbox'
      ]
    })
    console.log(`Iniciando o Chromium...`)
    // Abre o browser e acessa o site
    console.log(`Levantando os dados do modelo ${content.model} em ${content.cluster}...`)
    const page = await browser.newPage()
    const sigla = content.cluster
    const modelo = content.model.replace(" ", "%20")
    const version = content.version.replace(" ", "%20")
    const url = `http://datacenter.virtua.com.br/inventario/mac_desatualizado.php?sigla=${sigla}&modelo=${modelo}&sw_rev=${version}&data=${timestampData()}`
    await page.setViewport({ width: 1366, height: 768, deviceScaleFactor: 1 })
    await page.authenticate({username:`${process.env.LOGIN}`, password:`${process.env.PASSWORD}`})
    console.log(`Acessando o site Datacenter...`)
    await page.goto(`${url}`)
    await page.waitForTimeout(1000)
    const data = await page.evaluate(() => {
      const rows = Array.from(document.querySelectorAll('table[id="tabela"] > tbody > tr '))
      return Array.from(rows, row => {
        const columns = row.querySelectorAll('td')
        return Array.from(columns, column => column.innerText)
      })
    })
    let csv = ""
    for (let i = 0; i < data.length; i++) {
      csv += data[i][0] + `;`
      csv += data[i][1] + `;`
      csv += data[i][2] + `;`
      csv += data[i][3] + `\n`
    }
    fs.writeFileSync(`./${content.model}_${content.cluster}.csv`, csv)
    // Fecha o navegador
    console.log(`Encerrando o Chromium...`)
    await browser.close()
    console.log(`Finalizando a consulta...`)
  }catch(err){
    console.log(err)
  }
}

async function getMACTerminal(){
  console.log('[getMACTerminal] Starting...')
  robots.input()
  const content = await robots.state.load()
  //console.log(timestampData())
  acessaDatacenter()
}

getMACTerminal()
.then((value) => {
})
.catch((error) => console.log(error))
