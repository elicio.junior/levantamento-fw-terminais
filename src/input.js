const readline = require('readline-sync')
const state = require('./state.js')

function inputModel() {
  const content = {}
    content.cluster = askAndReturnCluster()
    content.model   = askAndReturnSearchTerm()
    content.version = askAndReturnVersion()
    //content.vendor  = askAndReturnVendor()
    state.save(content)

  function askAndReturnSearchTerm(){
    const userModel = readline.question('Entre com o modelo: ')
    const responseModel = userModel.toUpperCase()
    return responseModel
  }

  function askAndReturnVendor() {
    const userVendor = readline.question('Entre com o modelo: ')
    const responseVendor = userVendor.toUpperCase()
    return responseVendor
  }

  function askAndReturnVersion() {
    const userVersion = readline.question('Entre com a versao de FW atualizada: ')
    const responseVersion  = userVersion.toUpperCase()
    return responseVersion
  }

  function askAndReturnCluster() {
    const cluster = ['sts', 'sve', 'gja', 'bert', 'sjc', 'tte', 'pba', 'ada']
    const selectedClusterIndex = readline.keyInSelect(cluster)
    const selectedClusterName  = cluster[selectedClusterIndex]
    
    return selectedClusterName
  }
  console.log(content)
}

//inputModel()
module.exports = inputModel