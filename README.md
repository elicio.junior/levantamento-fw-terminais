# Levantamento FW Terminais

Sistema para coleta de firmware de terminais dentro da LDAP.

### Features

- Baixa o consolidado de firmware atualizados dos terminais da operação;
- Baixa um consolidado de firmware atualizados por cidade (baseado no arquivo dados-cidade.json);
- Baixa os macs dos terminais desatualizados (individual por cidade);
- Todas as exportações serão salvas na raiz do projeto em arquivo CSV;

# How To Use

####Requisitos do Sistema

 - Ter o NodeJS e o NPM instalados.

Baixar e instalar a última versão do NodeJS em [Download NodeJS](https://nodejs.org/en/download/)

Tutorial de como fazê-lo [aqui](https://nodejs.org/en/download/package-manager/)

####Instalação dos pacotes

`$ npm install`

####Consultar os firmwares atualizados e baixar o consolidado das cidades

`$ npm run start`
    
####Consultar os macs desatualizados de um determinado modelo

 `$ npm run mac`
